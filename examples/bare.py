
#
# mainHem_bare.py
# Skeleton app that can be used for app deployments

# Author: Ashray Manur

import datetime
import threading
import time
import sys

sys.path.insert(1,'../')
from module.hemSuperClient import HemSuperClient

#Create a new HEM Client. This is used to send and receive data from your HEM
# It takes two arguments - IP/hostname and port
# If you're deploying your app on the same HEM you're getting data from use localhost.Otherwise give IP address
#Port is usually 9931
hemSuperClient = HemSuperClient("localhost", 9931)

#This is function which gets triggered whenever you get data from server
#You can add more logic here for post processing
def update(message, address):
	print 'Rec:', message, address
	#{u'NODE': u'ALL', u'TYPE': u'DCPOWER', u'VALUE': [0.185, 5.9, 85.6, 10.4, 0, 0, 0, 12.5]} ('192.168.1.236', 9931)
	#message is a list which gives you the type of response and the corresponding nodes 
	#address is a tuple giving you the server address and the port

#Subscribe to data from HEMs
# The argument to this is the name of the function you want triggered when you get data
hemSuperClient.subscribe(update)

def main():

	while(1):

		#This sends a request to HEM every 5 seconds
		#Argument is the APIs
		hemSuperClient.sendRequest("api/getacpoweractive/all")
		time.sleep(5)

if __name__ == "__main__":
    main() 
